---
title: 'CytoMAP Publication'
date: 2020-04-21
permalink: /posts/2020-04-cytomappub/
tags:
  - publications
---



My paper describing my CytoMAP software was published in [Cell Reports!](https://www.cell.com/cell-reports/fulltext/S2211-1247(20)30423-X?_returnURL=https%3A%2F%2Flinkinghub.elsevier.com%2Fretrieve%2Fpii%2FS221112472030423X%3Fshowall%3Dtrue) It was even featured on the cover!

