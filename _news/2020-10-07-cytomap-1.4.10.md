---
title: 'CytoMAP Version 1.4.10'
date: 2020-10-07
permalink: /posts/CytoMAP V 1.4.10/
tags:
  - CytoMAP
  - Version 1.4.10
  - Exporting Data
---

CytoMAP version 1.4.10 is now live. Download it as a [MATLAB app](https://www.mathworks.com/matlabcentral/fileexchange/75348-cytomap?s_tid=prof_contriblnk), or a standalone [compiled version](https://gitlab.com/gernerlab/cytomap/-/blob/master/StandaloneInstaller/CytoMAP_Installer_WindowsV1.4.10.exe). 

---

Changes for this version include:

- Plot Gates no longer fills in the points by default.
- Added the option in plotted gates to right click and enable fill in gated points
- Fixed issues with NaNs and missing values when defining neighborhoods
- Changed import multiple samples to default to no Z axis if no z axis is chosen on first sample
- Added plot titles to cluster results
- Fixed a plot rendering issue in plots of clustered cell results
- Fixed index issue when plotting region statistics for manually defined regions
- Fixed indexing error in cell heatmaps for combined samples