---
title: 'CytoMAP Version 1.4.21'
date: 2021-06-29
permalink: /posts/CytoMAP V 1.4.21/
tags:
  - CytoMAP
  - Version 1.4.21
---

CytoMAP version 1.4.21 is now live. Download it as a [MATLAB app](https://www.mathworks.com/matlabcentral/fileexchange/75348-cytomap?s_tid=prof_contriblnk), or a standalone [compiled version](https://gitlab.com/gernerlab/cytomap/-/blob/master/StandaloneInstaller/CytoMAP_Installer_WindowsV1.4.21.exe). 

---

Changes for this version include:

- Added option to change overlap threshold in region interaction plots 
- Made the fit plots in density model smoother and added overlay of fit functions 
- Added a try statement to load .wsp to deal with derived parameter names

