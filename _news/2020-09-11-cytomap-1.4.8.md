---
title: 'CytoMAP Version 1.4.8'
date: 2020-09-11
permalink: /posts/CytoMAP V 1.4.8/
tags:
  - CytoMAP
  - Version 1.4.8
  - Exporting Data
---

CytoMAP version 1.4.8 is now live. Download it as a [MATLAB app](https://www.mathworks.com/matlabcentral/fileexchange/75348-cytomap?s_tid=prof_contriblnk), or a standalone [compiled version](https://gitlab.com/gernerlab/cytomap/-/blob/master/StandaloneInstaller/CytoMAP_Installer_WindowsV1.4.8.exe). 

---

Changes for this version include:

- Added more options to the data export functionality 
- Fixed indexing problems in the Annotate Clusters GUI