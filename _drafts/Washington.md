---
layout: single
title:  "Exploring Washington"
header:
categories: 
  - Exploration
tags:
  - Washington
  - Nature
---
Living in Seattle has introduced me to a whole different outdoor experience. The ocean views are amazing and the lush forests make any hiking experience a joy.

