---
layout: single
title:  "Meet My Cats"
date: 2020-08-13
permalink: /posts/2020/08/Pets/
header:
categories: 
  - Pets
tags:
  - Pets
  - Cats
---
[![Both](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Cats01.jpg)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Cats01.jpg)

Salem (left) and Herman (right) enjoy napping and zooming about the house at 3am. 

|                                                              |                                                              |
| ------------------------------------------------------------ | :----------------------------------------------------------- |
| Salem loves new people and makes fast friends. She is always interested in what you are doing. She views keyboards as the perfect place to nap. | [![Salem](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Cats02.jpg)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Cats02.jpg) |
| [![Herman](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Cats03.jpg)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Cats03.jpg) | Herman likes to climb on top of anything he can reach. He pretends to be aloof but will follow you anywhere. |



