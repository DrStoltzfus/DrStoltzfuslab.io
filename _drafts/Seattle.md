---
layout: single
title:  "Exploring Seattle"
header:
categories: 
  - Exploration
tags:
  - Seattle
  - City Scape
---
Although I prefer hiking out in the middle of nowhere I have grown to enjoy the urban jungle. The views of the city at night are nice
