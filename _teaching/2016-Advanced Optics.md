---
title: "Advanced Optics (TA)"
collection: teaching
type: "Undergraduate course"
permalink: /teaching/2016-Advanced Optics
venue: "Montana State University, Department of Physics"
date: 2016-01-01
location: "Bozeman MT, USA"
---

Under the supervision of [Professor Rufus Cone](http://physics.montana.edu/directory/faculty/1524001/rufus-cone) I graded homework and held office hours. This course covered new developments in optics laser physics.  Provided a good foundation in wave optics, nonlinear optics, integrated  optics, and spectroscopy.

