---
title: "Holography Lab (TA)"
collection: teaching
type: "Undergraduate course"
permalink: /teaching/2010-spring-Holography Lab
venue: "Montana State University, Department of Physics"
date: 2010-01-01
location: "Bozeman MT, USA"
---

Under the supervision of [Professor Randy Babbitt](http://www.physics.montana.edu/directory/faculty/1523879/williamrandall-babbitt) I taught lab sections demonstrating how to make holograms. I occasionally also taught lectures.