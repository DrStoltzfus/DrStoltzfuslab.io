---
title: "Introductory Physics (TA)"
collection: teaching
type: "Undergraduate course"
permalink: /teaching/2016-IntoPhysics
venue: "Montana State University, Department of Physics"
date: 2016-01-01
location: "Bozeman MT, USA"
---

Under the supervision of [Profesor Gregory Francis](http://physics.montana.edu/directory/faculty/1524092/gregory-francis) I taught a lab section, graded homework, and graded exams. This introductory physics course covered simple harmonic motion; electric forces and fields; dc  electric circuits; magnetic forces and fields; and magnetic induction and motors.