---
title: "Computational Methods (TA)"
collection: teaching
type: "Undergraduate course"
permalink: /teaching/2015-Mathematica
venue: "Montana State University, Department of Physics"
date: 2015-01-01
location: "Bozeman MT, USA"
---

Under the supervision of [Professor Aleks Rebane](http://physics.montana.edu/directory/faculty/1524505/aleksander-rebane) I taught occasional lectures, and held office hours. 

