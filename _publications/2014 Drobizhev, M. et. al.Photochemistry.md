---
title: "Multiphoton Photochemistry of Red Fluorescent Proteins in Solution and Live Cells"
collection: publications
permalink: /publication/Drobizhev,M.et.al.2014_2
excerpt:
date: 2014-07-08
venue: 'The Journal of Physical Chemistry B'
paperurl: 'https://doi.org/10.1021/jp502477c'
citation: 'Drobizhev, Mikhail, Caleb Stoltzfus, Igor Topol, Jack Collins, Geoffrey Wicks, Alexander Mikhaylov, Lauren Barnett, Thomas E. Hughes, and Aleksander Rebane. "Multiphoton photochemistry of red fluorescent proteins in solution and live cells." The Journal of Physical Chemistry B 118, no. 31 (2014): 9167-9179.'
---

## Abstract

Genetically encoded fluorescent proteins (FPs), and biosensors based on them,  provide new insights into how living cells and tissues function.  Ultimately, the goal of the bioimaging community is to use these probes  deep in tissues and even in entire organisms, and this will require  two-photon laser scanning microscopy (TPLSM), with its greater tissue  penetration, lower autofluorescence background, and minimum photodamage  in the out-of-focus volume. However, the extremely high instantaneous  light intensities of femtosecond pulses in the focal volume dramatically increase the probability of further stepwise resonant photon  absorption, leading to highly excited, ionizable and reactive states,  often resulting in fast bleaching of fluorescent proteins in TPLSM.  Here, we show that the femtosecond multiphoton excitation of red FPs  (DsRed2 and mFruits), both in solution and live cells, results in a  chain of consecutive, partially reversible reactions, with individual  rates driven by a high-order (3–5 photon) absorption. The first step of  this process corresponds to a three- (DsRed2) or four-photon (mFruits)  induced fast isomerization of the chromophore, yielding intermediate  fluorescent forms, which then subsequently transform into nonfluorescent products. Our experimental data and model calculations are consistent  with a mechanism in which ultrafast electron transfer from the  chromophore to a neighboring positively charged amino acid residue  triggers the first step of multiphoton chromophore transformations in  DsRed2 and mFruits, consisting of decarboxylation of a nearby  deprotonated glutamic acid residue.

