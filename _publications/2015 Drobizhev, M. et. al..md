---
title: "Long- and Short-Range Electrostatic Fields in GFP Mutants: Implications for Spectral Tuning"
collection: publications
permalink: /publication/Drobizhev,M.et.al.2015
excerpt:
date: 2015-08-19
venue: 'Scientific Reports'
paperurl: 'https://doi.org/10.1038/srep13223'
citation: 'Drobizhev, M., P. R. Callis, R. Nifosì, G. Wicks, C. Stoltzfus, L. Barnett, T. E. Hughes, P. Sullivan, and A. Rebane. "Long-and short-range electrostatic fields in GFP mutants: Implications for spectral tuning." Scientific Reports 5, no. 1 (2015): 1-14.'
---

## Abstract

The majority of protein functions are governed by their internal local  electrostatics. Quantitative information about these interactions can  shed light on how proteins work and allow for improving/altering their  performance. Green fluorescent protein (GFP) and its mutation variants  provide unique optical windows for interrogation of internal electric  fields, thanks to the intrinsic fluorophore group formed inside them.  Here we use an all-optical method, based on the independent measurements of transition frequency and one- and two-photon absorption cross  sections in a number of GFP mutants to evaluate these internal electric  fields. Two physical models based on the quadratic Stark effect, either  with or without taking into account structural (bond-length) changes of  the chromophore in varying field, allow us to separately evaluate the  long-range and the total effective (short- and long-range) fields. Both  types of the field quantitatively agree with the results of independent  molecular dynamic simulations, justifying our method of measurement.

