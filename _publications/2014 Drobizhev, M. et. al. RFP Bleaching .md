---
title: "Mechanisms of Multiphoton Bleaching of Red Fluorescent Proteins"
collection: publications
permalink: /publication/Drobizhev,M.et.al.2014_1
excerpt:
date: 2014-02-18
venue: 'Biophysical Journal'
paperurl: 'https://doi.org/10.1016/j.bpj.2013.11.3356'
citation: 'Drobizhev, Mikhail, Caleb Stoltzfus, Thomas Hughes, Igor Topol, Lauren M. Barnett, Geoffrey R. Wicks, and Aleksander Rebane. "Mechanisms of Multiphoton Bleaching of Red Fluorescent Proteins." Biophysical Journal 106, no. 2 (2014): 607a.'
---


## Abstract

Two-photon laser scanning microscopy (TPLSM) has several advantages over one-photon confocal microscopy, including deeper tissue penetration, higher signal-to-background ratio, and less photodamage in the out-of-focus volume. However, due to very high instantaneous light intensities in the focal volume,
the probability of further, stepwise resonant photon(s) absorption increases dramatically, leading to very efficient bleaching of a probe. To deal with this challenge one has to understand the underlying mechanisms. Here we measured the power dependence of multiphoton bleaching rates of several red fluorescent proteins expressed in live E. coli cells under two-photon microscope conditions. To clarify the photophysical mechanisms, we also used much lower repetition rate (1 kHz) and different pulse durations in experiments in vitro with Ti:Sa amplifier excitation. Our experimental data supported by quantum mechanical calculations of the chromophore in protein environment are consistent with the mechanism of ultrafast (<150 fs) singlet-singlet stepwise absorption of one or two additional photons following initial simultaneous two-photon absorption. In the DsRed2 protein, the third photon absorption most probably results in an ultrafast electron transfer (ET) from the anionic chromophore to an excited Rydberg state of a nearby positive amino acid residue (e.g. K163þ). The transient radical state of the chromophore tends to accept an electron from the deprotonated E215- amino acid, thus promoting the first step of the recently established decarboxylation reaction. In mFruits proteins, the third photon promotes an ET from the chromophore to the 4S Rydberg state of nearby K70þ residue with its subsequent photoionization by the fourth photon.

[Download paper here](https://drstoltzfus.gitlab.io/files/Drobizhev, M. Mechanisms of multiphoton bleaching of red fluorescent proteins, (2014).pdf)

