---
title: "Wide Field-of-View Two-Photon Excited Fluorescence Imaging, Theory and Applications"
collection: publications
permalink: /publication/Stoltzfus,C.et.al.2016_2
date: 2016-04-30
venue: 'Montana State University-Bozeman, College of Letters & Science'
paperurl: 'https://scholarworks.montana.edu/xmlui/handle/1/9861'
citation: 'Stoltzfus, Caleb Ray. "Wide field of view two-photon excited fluorescence imaging, theory and applications." PhD diss., Montana State University-Bozeman, College of Letters & Science, 2016.'
---


## Abstract

Two-photon excited fluorescence (2PEF) is a unique photophysical process that has benefited many diverse areas of science. Imaging the 2PEF  signal offers numerous intrinsic benefits, including low background  scattering, high sample photo-stability, and high excitation  selectivity. The 2PEF signal has a nonlinear dependence on excitation  intensity, which has proven to be extremely useful for high resolution,  three dimensional microscopy. This same nonlinear dependence, in  conjunction with the typically low probability of two-photons being  simultaneously absorbed, also makes 2PEF imaging difficult to scale,  leaving most two-photon microscopes with a field of view (FOV) limited  to less than a few mm 2. This effectively limits the benefits of the  unique properties of 2PEF imaging to microscopic applications. This  dissertation explores the development and application of a wide FOV 2PEF imaging technique, where a FOV as large as 10 cm 2 is achieved by  increasing the peak photon flux of the excitation source, and expanding  the illumination region. The use of this imaging technique for the in  depth characterization and optimization of fluorescent proteins (FPs),  as well as taking high contrast images of fingermarks is described. This new wide FOV 2PEF imaging technique greatly expands the usefulness of  the unique photophysical properties of 2PEF and allows for sensitive,  high contrast 2PEF imaging on a much larger scale than was previously  possible.

[Download paper here](https://drstoltzfus.gitlab.io/files/Stoltzfus, C. Micro-sized tunable liquid crystal optical filters, 2017.pdf)

