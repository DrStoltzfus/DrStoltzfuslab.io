---
title: "The Two-Photon Bazooka: A New Way of Optically Screening Randomly Mutagenized Libraries of Fluorescent Proteins"
collection: publications
permalink: /publication/Barnett,M.et.al.2014
excerpt:
date: 2014-02-19
venue: 'Biophysical Journal'
paperurl: 'https://doi.org/10.1016/j.bpj.2013.11.4445'
citation: 'Barnett, Lauren M., Caleb Stoltzfus, Geoffrey Wicks, Mikhail Drobizhev, Alexandr Mikhailov, Aleksander Rebane, and Thomas E. Hughes. "The two-photon bazooka: A new way of optically screening randomly mutagenized libraries of fluorescent proteins." Biophysical Journal 106, no. 2 (2014): 810a-811a.'
---


## Abstract

Two-photon (2P) microscopy is the preferred method for imaging fluorescent proteins and biosensors in living, thick tissues. Over the past decade, many groups have worked to improve single photon (1P) properties of fluorescent proteins, but little has been done to improve their 2P properties. This is important because 2P properties can be quite different from the 1P properties. Our goal was to create a system for screening libraries of randomly mutagenized fluorescent proteins for better 2P properties. We created an optical system for collecting 1- and 2-photon induced fluorescence images from entire petri dishes of E. coli colonies. Libraries of randomly mutagenized eGFP were screened for improved 2P/1P fluorescence ratios. Screening the first round of evolution showed a slightly larger ratio spread outside the range of parent eGFP. One hundred of these clones were subjected to a second round of screening. Colonies that showed reproducible 2P/1P ratios greater than the parent eGFP were selected to move forward. A selective pool of the best 18 clones (library A), and a more inclusive pool of the best 59 clones (library B), were used for a new round of gene shuffling and random mutagenesis. After only two rounds of evolution, libraries A and B contained at least 5 distinct mutant populations with significantly increased 2P/1P ratios. Sequence analysis of 96 clones from these libraries revealed that every clone harbored at least one mutation. Eleven unique mutants were used for a third round of evolution. Screening and sequence analysis of the third library, reveals thirteen mutations that either individually (V68M, S72G, V163A, T203I) or in combination (E6G, T65S, Q80R, N105S, D117G, N121S, Q184R, S202N, V219I) significantly shift the 2P/1P fluorescence ratios.