---
title: "Optimizing Ultrafast Illumination for Multiphoton-Excited Fluorescence Imaging "
collection: publications
permalink: /publication/Stoltzfus,et.al.2016_3
excerpt: 
date: 2016-04-07
venue: 'Biomedical Optics Express'
paperurl: 'https://doi.org/10.1364/BOE.7.001768'
citation: 'Stoltzfus, Caleb R., and Aleksander Rebane. "Optimizing ultrafast illumination for multiphoton-excited fluorescence imaging." Biomedical Optics Express 7, no. 5 (2016): 1768-1782.'
---



## Abstract

We study the optimal conditions for high throughput two-photon excited  fluorescence (2PEF) and three-photon excited fluorescence (3PEF) imaging using femtosecond lasers. We derive relations that allow maximization  of the rate of imaging depending on the average power, pulse repetition  rate, and noise characteristics of the laser, as well as on the size and structure of the sample. We perform our analysis using ~100 MHz, ~1 MHz and 1 kHz pulse rates and using both a tightly-focused illumination  beam with diffraction-limited image resolution, as well loosely focused  illumination with a relatively low image resolution, where the latter  utilizes separate illumination and fluorescence detection beam paths.  Our theoretical estimates agree with the experiments, which makes our  approach especially useful for optimizing high throughput imaging of  large samples with a field-of-view up to 10x10 cm2.

[Download paper here](https://drstoltzfus.gitlab.io/files/Stoltzfus, C. Optimizing ultrafast illumination for multiphoton-excited fluorescence imaging, 2016.pdf)

