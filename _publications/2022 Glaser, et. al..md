---
title: "A hybrid open-top light-sheet microscope for versatile multi-scale imaging of cleared tissues"
collection: publications
permalink: /publication/Glaser,et.al.2022
excerpt: 
date: 2022-05-11
venue: 'Nature Methods'
paperurl: 'https://doi.org/10.1038/s41592-022-01468-5'
citation: 'Glaser, Adam K., Bishop, Kevin W., Barner, Lindsey A., Susaki, Etsuo A., Kubota, Shimpei I., Gao, Gan, Serafin, Robert B., Balaram, Pooja, Turschak, Emily, Nicovich, Philip R., Lai, Hoyin, Lucas, Luciano A. G., Yi, Yating, Nichols, Eva K., Huang, Hongyi, Reder, Nicholas P., Wilson, Jasmine J., Sivakumar, Ramya, Shamskhou, Elya, Stoltzfus, Caleb R., Wei, Xing, Hempton, Andrew K., Pende, Marko, Murawala, Prayag, Dodt, Hans-Ulrich, Imaizumi, Takato, Shendure, Jay, Beliveau, Brian J., Gerner, Michael Y., Xin, Li, Zhao, Hu, True, Lawrence D., Reid, Clay R., Chandrashekar, Jayaram, Ueda, Hiroki R., Svoboda, Karel, Lui, Jonathan T.C. "A hybrid open-top light-sheet microscope for versatile multi-scale imaging of cleared tissues." Nature Methods 19, no. 5 (2022): 613-619.'
---


## Abstract

Light-sheet microscopy has emerged as the preferred means for high-throughput volumetric imaging of cleared tissues. However, there is a need for a flexible system that can address imaging applications with varied requirements in terms of resolution, sample size, tissue-clearing protocol, and transparent sample-holder material. Here, we present a ‘hybrid’ system that combines a unique non-orthogonal dual-objective and  conventional (orthogonal) open-top light-sheet (OTLS) architecture for versatile multi-scale volumetric imaging. We demonstrate efficient screening and targeted sub-micrometer imaging of sparse axons within an intact, cleared mouse brain. The same system enables high-throughput automated imaging of multiple specimens, as spotlighted by a quantitative multi-scale analysis of brain metastases. Compared with existing academic and commercial light-sheet microscopy systems, our hybrid OTLS system provides a unique combination of versatility and performance necessary to satisfy the diverse requirements of a growing number of cleared-tissue imaging applications.



