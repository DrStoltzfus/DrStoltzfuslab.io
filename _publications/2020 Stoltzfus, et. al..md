---
title: "CytoMAP: A Spatial Analysis Toolbox Reveals Features of Myeloid Cell Organization in Lymphoid Tissues"
collection: publications
permalink: /publication/Stoltzfus,et.al.2020
excerpt: 
date: 2020-04-21
venue: 'Cell Reports'
paperurl: 'https://doi.org/10.1016/j.celrep.2020.107523'
citation: 'Stoltzfus, Caleb R., Filipek, Jakub, Gern, Benjamin H., Olin, Brandy E., Leal, Joseph M., Wu, Yajun, Lyons-Cohen, Miranda R. et al. "CytoMAP: a spatial analysis toolbox reveals features of myeloid cell organization in lymphoid tissues." Cell Reports 31, no. 3 (2020): 107523.'
---


## Abstract

Recently developed approaches for highly multiplexed imaging have  revealed complex patterns of cellular positioning and cell-cell  interactions with important roles in both cellular- and tissue-level  physiology. However, tools to quantitatively study cellular patterning  and tissue architecture are currently lacking. Here, we develop a  spatial analysis toolbox, the histo-cytometric multidimensional analysis pipeline (CytoMAP), which incorporates data clustering, positional  correlation, dimensionality reduction, and 2D/3D region reconstruction  to identify localized cellular networks and reveal features of tissue  organization. We apply CytoMAP to study the microanatomy of innate  immune subsets in murine lymph nodes (LNs) and reveal mutually exclusive segregation of migratory dendritic cells (DCs), regionalized  compartmentalization of SIRPα− dermal DCs, and preferential  association of resident DCs with select LN vasculature. The findings  provide insights into the organization of myeloid cells in LNs and  demonstrate that CytoMAP is a comprehensive analytics toolbox for  revealing features of tissue organization in imaging datasets.

[Download paper here](https://drstoltzfus.gitlab.io/files/Stoltzfus, C. CytoMAP; A spatial analysis toolbox reveals features of myeloid cell organization in lymphoid tissues, 2020.pdf)

