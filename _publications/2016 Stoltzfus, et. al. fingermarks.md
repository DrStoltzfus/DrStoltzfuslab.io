---
title: "High Contrast Two-Photon Imaging of Fingermarks"
collection: publications
permalink: /publication/Stoltzfus,C.et.al.2016_1
excerpt: 
date: 2016-04-07
venue: 'Scientific Reports'
paperurl: 'https://doi.org/10.1038/srep24142'
citation: 'Stoltzfus, Caleb R., and Aleksander Rebane. "High contrast two-photon imaging of fingermarks." Scientific Reports 6, no. 1 (2016): 1-4.'
---


## Abstract

Optically-acquired fingermarks are widely used as evidence across law  enforcement agencies as well as in the courts of law. A common technique for visualizing latent fingermarks on nonporous surfaces consists of  cyanoacrylate fuming of the fingerprint material, followed by  impregnation with a fluorescent dye, which under ultra violet (UV)  illumination makes the fingermarks visible and thus accessible for  digital recording. However, there exist critical circumstances, when the image quality is compromised due to high background scattering, high  auto-fluorescence of the substrate material, or other detrimental  photo-physical and photo-chemical effects such as light-induced damage  to the sample. Here we present a novel near-infrared (NIR), two-photon  induced fluorescence imaging modality, which significantly enhances the  quality of the fingermark images, especially when obtained from highly  reflective and/or scattering surfaces, while at the same time reducing  photo-damage to sensitive forensic samples.

[Download paper here](https://drstoltzfus.gitlab.io/files/Stoltzfus, C. High contrast two-photon imaging of fingermarks, 2016.pdf)

