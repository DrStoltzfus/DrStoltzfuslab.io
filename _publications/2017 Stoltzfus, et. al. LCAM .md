---
title: "Micro-Sized Tunable Liquid Crystal Optical Filters"
collection: publications
permalink: /publication/Stoltzfus,et.al.2017_2
excerpt: 
date: 2017-04-30
venue: 'Optics Letters'
paperurl: 'https://doi.org/10.1364/OL.42.002090'
citation: 'Stoltzfus, Caleb, Russell Barbour, David Atherton, and Zeb Barber. "Micro-sized tunable liquid crystal optical filters." Optics Letters 42, no. 11 (2017): 2090-2093.'
---


## Abstract

Liquid crystal arrayed microcavities (LCAM) is a new technology for ultra-narrow optical filtering (FWHM ∼0.1  nm) that uses picoliter volume Fabry–Perot-type optical cavities filled  with liquid crystal for tuning. LCAMs are sub-nm spectral resolution  filters, which utilize well-established laser writing, thin film  deposition, and wafer manufacturing techniques. These filters are  compact, robust, and inexpensive. Compact, high-resolution optical  filters have applications, including biomedical imaging, chemical  detection, and environmental monitoring. Here, we describe the LCAM  design and initial performance metrics.

[Download paper here](https://drstoltzfus.gitlab.io/files/Stoltzfus, C. Micro-sized tunable liquid crystal optical filters, 2017.pdf)

