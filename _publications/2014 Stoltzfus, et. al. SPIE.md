---
title: "A Multidimensional Screening Method for the Selection of Two-Photon Enhanced Fluorescent Proteins"
collection: publications
permalink: /publication/Stoltzfus,C.et.al.2014
excerpt: 
date: 2014-03-05
venue: 'International Society for Optics and Photonics'
paperurl: 'https://doi.org/10.1117/12.2039873'
citation: 'Stoltzfus, Caleb, Lauren Barnett, Aleksander Rebane, Thomas Hughes, Mikhail Drobizhev, Geoffrey Wicks, and Alexandr Mikhailov. "A multidimensional screening method for the selection of two-photon enhanced fluorescent proteins." In Reporters, Markers, Dyes, Nanoparticles, and Molecular Probes for Biomedical Applications VI, vol. 8956, p. 895611. International Society for Optics and Photonics, 2014.'
---

## Abstract

Two-photon excitation of fluorescent proteins (FPs) is widely used in  imaging whole organisms or living tissues. Many different FPs are now  available but these proteins have only been optimized for their  one-photon properties. We have developed a technique for screening  entire libraries of E. coli colonies expressing FPs that utilizes  multiple wavelengths of linear excitation as well as two-photon  excitation. Single mutations in a particular protein that affect one or  twophoton properties are easily identified, providing new views of  structure/function relationships. An amplified femtosecond Ti:sapphire  laser and a spectrally filtered lamp source are used to acquire the  fluorescence signals of up to ~1000 E. coli colonies on a standard Petri dish. Automation of the analysis and acquisition of the fluorescent  signals makes it feasible to rapidly screen tens of thousands of  colonies. In a proof of principle experiment with the commonly used  EGFP, we used two rounds of error prone PCR and selection to evolve new  proteins with shifted absorption and increased two-photon cross sections at 790nm. This method of screening, coupled with careful measurements  of photo bleaching dynamics and two-photon cross sections, should make  it possible to optimize a wide variety of fluorescent proteins and  biosensors for use in two-photon microscopes.