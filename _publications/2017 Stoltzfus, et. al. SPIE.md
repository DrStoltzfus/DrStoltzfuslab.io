---
title: "Optimizing Ultrafast Wide Field-of-View Illumination for High-Throughput Multi-Photon Imaging and Screening of Mutant Fluorescent Proteins"
collection: publications
permalink: /publication/Stoltzfus,et.al.2017_1
excerpt: 
date: 2017-02-21
venue: 'International Society for Optics and Photonics'
paperurl: 'https://doi.org/10.1117/12.2250048'
citation: 'Stoltzfus, Caleb, Alexandr Mikhailov, and Aleksander Rebane. "Optimizing ultrafast wide field-of-view illumination for high-throughput multi-photon imaging and screening of mutant fluorescent proteins." In Multiphoton Microscopy in the Biomedical Sciences XVII, vol. 10069, p. 1006924. International Society for Optics and Photonics, 2017.'
---


## Abstract

Fluorescence induced by 1wo-photon absorption (2PA) and three-photon  absorption (3PA) is becoming an increasingly important tool for  deep-tissue microscopy, especially in conjunction with  genetically-encoded functional probes such as fluorescent proteins  (FPs). Unfortunately, the efficacy of the multi-photon excitation of FPs is notoriously low, and because relations between a biological  fluorophore’s nonlinear-optical properties and its molecular structure  are inherently complex, there are no practical avenues available that  would allow boosting the performance of current FPs. Here we describe a  novel method, where we apply directed evolution to optimize the 2PA  properties of EGFP. Key to the success of this approach consists in  high-throughput screening of mutants that would allow selection of  variants with promising 2PA and 3PA properties in a broad near-IR  excitation range of wavelength. For this purpose, we construct and test a wide field-of-view (FOV), femtosecond imaging system that we then use  to quantify the multi-photon excited fluorescence in the 550- 1600 nm  range of tens of thousands of E. coli colonies expressing randomly  mutated FPs in a standard 10 cm diameter Petri dish configuration. We  present a quantitative analysis of different factors that are currently  limiting the maximum throughput of the femtosecond multi-photon  screening techniques and also report on quantitative measurement of  absolute 2PA and 3PA cross sections spectra.