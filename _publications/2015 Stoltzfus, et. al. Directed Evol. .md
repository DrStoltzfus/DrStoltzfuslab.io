---
title: "Two-Photon Directed Evolution of Green Fluorescent Proteins"
collection: publications
permalink: /publication/Stoltzfus,C.et.al.2015
excerpt: 
date: 2015-07-06
venue: 'Scientific Reports'
paperurl: 'https://doi.org/10.1038/srep11968'
citation: 'Stoltzfus, Caleb R., Lauren M. Barnett, Mikhail Drobizhev, Geoffrey Wicks, Alexander Mikhaylov, Thomas E. Hughes, and Aleksander Rebane. "Two-photon directed evolution of green fluorescent proteins." Scientific Reports 5 (2015): 11968.'
---

## Abstract

Directed evolution has been used extensively to improve the properties of a  variety of fluorescent proteins (FPs). Evolutionary strategies, however, have not yet been used to improve the two-photon absorption (2PA)  properties of a fluorescent protein, properties that are important for  two-photon imaging in living tissues, including the brain. Here we  demonstrate a technique for quantitatively screening the two-photon  excited fluorescence (2PEF) efficiency and 2PA cross section of tens of  thousands of mutant FPs expressed in *E. coli* colonies. We use  this procedure to move EGFP through three rounds of two-photon directed  evolution leading to new variants showing up to a 50% enhancement in  peak 2PA cross section and brightness within the near-IR tissue  transparency wavelength range.