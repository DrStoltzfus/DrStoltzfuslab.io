---
title: "Ultra-low Dose Aerosol Infection of Mice with Mycobacterium tuberculosis More Closely Models Human Tuberculosis"
collection: publications
permalink: /publication/Plumlee,et.al.,2021
date: 2021-01-13
venue: 'Cell Host & Microbe'
paperurl: 'https://doi.org/10.1016/j.chom.2020.10.003'
citation: 'Plumlee, Courtney R., Duffy, Fergal J., Gern, Benjamin H., Delahaye, Jared L., Cohen, Sara B., Stoltzfus, Caleb R., Rustad, Tige R. et al. "Ultra-low Dose Aerosol Infection of Mice with Mycobacterium tuberculosis More Closely Models Human Tuberculosis" Cell Host & Microbe 29, no. 1 (2021): 68-82.'
---

### Abstract

Tuberculosis (TB) is a heterogeneous disease manifesting in a subset of individuals infected with aerosolized *Mycobacterium tuberculosis* (Mtb). Unlike human TB, murine infection results in uniformly high lung bacterial burdens and poorly organized granulomas. To develop a TB  model that more closely resembles human disease, we infected mice with  an ultra-low dose (ULD) of between 1–3 founding bacteria, reflecting a  physiologic inoculum. ULD-infected mice exhibited highly heterogeneous  bacterial burdens, well-circumscribed granulomas that shared features  with human granulomas, and prolonged Mtb containment with unilateral  pulmonary infection in some mice. We identified blood RNA signatures in  mice infected with an ULD or a conventional Mtb dose (50–100 CFU) that  correlated with lung bacterial burdens and predicted Mtb infection  outcomes across species, including risk of progression to active TB in  humans. Overall, these findings highlight the potential of the murine TB model and show that ULD infection recapitulates key features of human  TB.