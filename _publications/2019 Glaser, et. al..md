---
title: "Multi-Immersion Open-Top Light-Sheet Microscope for High-Throughput Imaging of Cleared Tissues"
collection: publications
permalink: /publication/Glaser,et.al.2019
excerpt: 
date: 2019-07-04
venue: 'Nature Communications'
paperurl: 'https://doi.org/10.1038/s41467-019-10534-0'
citation: 'Glaser, Adam K., Reder, Nicholas P., Chen, Ye, Yin, Chengbo, Wei, Linpeng, Kang, Soyoung, Barner, Lindsey A. et al. "Multi-immersion open-top light-sheet microscope for high-throughput imaging of cleared tissues." Nature Communications 10, no. 1 (2019): 1-8.'
---


## Abstract

Recent advances in optical clearing and light-sheet microscopy have  provided unprecedented access to structural and molecular information  from intact tissues. However, current light-sheet microscopes have  imposed constraints on the size, shape, number of specimens, and  compatibility with various clearing protocols. Here we present a  multi-immersion open-top light-sheet microscope that enables simple  mounting of multiple specimens processed with a variety of clearing  protocols, which will facilitate wide adoption by preclinical  researchers and clinical laboratories. In particular, the open-top  geometry provides unsurpassed versatility to interface with a wide range of accessory technologies in the future.

[Download paper here](https://drstoltzfus.gitlab.io/files/Glaser, A. Multi-immersion open-top light-sheet microscope for high-throughput imaging of cleared tissues, 2019.pdf)

