---
title: "TGFβ restricts expansion, survival, and function of T cells within the tuberculous granuloma"
collection: publications
permalink: /publication/Gern,et.al.,2021
date: 2021-03-11
venue: 'Cell Host & Microbe'
paperurl: 'https://doi.org/10.1016/j.chom.2021.02.005'
citation: 'Gern, Benjamin H., Adams, Kristin N., Plumlee, Courtney R., Stoltzfus, Caleb R., Shehata, Laila, Moguche, Albanus O., Busman-Sahay, Kathleen et al. "TGFβ restricts expansion, survival, and function of T cells within the tuberculous granuloma." Cell Host & Microbe  (2021).'
---

### Abstract

CD4 T cell effector function is required for optimal containment of *Mycobacterium tuberculosis* (Mtb) infection. IFNɣ produced by CD4 T cells is a key cytokine that contributes to protection. However, lung-infiltrating CD4 T cells have a limited ability to produce IFNɣ, and IFNɣ plays a lesser protective role within the lung than at sites of Mtb dissemination. In a murine infection model, we observed that IFNɣ production by Mtb-specific CD4 T cells is rapidly extinguished within the granuloma but not within unaffected lung regions, suggesting localized immunosuppression. We identified a signature of TGFβ signaling within granuloma-infiltrating T cells in both mice and rhesus macaques. Selective blockade of TGFβ signaling in T cells resulted in an accumulation of terminally differentiated effector CD4 T cells, improved IFNɣ production within granulomas, and reduced bacterial burdens. These findings uncover a spatially localized immunosuppressive mechanism associated with Mtb infection and provide potential targets for host-directed therapy.


## Preprint

TGFβ restricts T cell function and bacterial control within the tuberculous granuloma

2020-02-26

DOI: https://doi.org/10.1101/696534 