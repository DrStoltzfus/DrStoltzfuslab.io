---
title: "Innate cell microenvironments in lymph nodes shape the generation of T cell responses during type I inflammation"
collection: publications
permalink: /publication/Leal,et.al.2021
excerpt: 
date: 2021-02-12
venue: 'Science Immunology'
paperurl: 'https://doi.org/10.1126/sciimmunol.abb9435'
citation: 'Leal, Joseph M., Huang, Jessica Y., Kohli, Karan, Stoltzfus, Caleb R., Lyons-Cohen, Miranda R., Olin, Brandy E., Gale Jr., Michael, Gerner, Michael Y. "Innate cell microenvironments in lymph nodes shape the generation of T cell responses during type I inflammation" Science Immunology 6, no. 56 (2021): eabb9435'
---


## Abstract

Microanatomical organization of innate immune cells within lymph nodes (LNs) is critical for the generation of adaptive responses. In  particular, steady-state LN-resident dendritic cells (Res cDCs) are  strategically localized to intercept lymph-draining antigens. Whether  myeloid cell organization changes during inflammation and how that might affect the generation of immune responses are unknown. Here, we report  that during type I, but not type II, inflammation after adjuvant  immunization or viral infection, antigen-presenting Res cDCs undergo  CCR7-dependent intranodal repositioning from the LN periphery into the T cell zone (TZ) to elicit T cell priming. Concurrently, inflammatory  monocytes infiltrate the LNs via local blood vessels, enter the TZ, and  cooperate with Res cDCs by providing polarizing cytokines to optimize T  cell effector differentiation. Monocyte infiltration is nonuniform  across LNs, generating distinct microenvironments with varied local  innate cell composition. These spatial microdomains are associated with  divergent early T cell effector programming, indicating that innate  microenvironments within LNs play a critical role in regulating the  quality and heterogeneity of T cell responses. Together, our findings  reveal that dynamic modulation of innate cell microenvironments during  type I inflammation leads to optimized generation of adaptive immune responses to vaccines and infections.

