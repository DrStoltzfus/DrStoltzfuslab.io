---
title: "Graduate Research; Rebane Lab (MSU)"
excerpt: "Wide Field-of-View Two-Photon Imaging <br/><a href='https://cstoltzfus.com/research/2016%20Rebane%20Lab/'><img src='/images/TwoPhotonImaging.PNG'></a>"
collection: research
---

**Aug. 2012 - Apr. 2016**  

In [Dr. Aleksander Rebane's lab](http://physics.montana.edu/directory/faculty/1524505/aleksander-rebane) I co-invented and optimized a wide field-of-view two-photon imaging system capable of capturing the  two-photon excited fluorescence of ~10 cm-sized samples. We used this  new imaging system combined with automated image analysis techniques I  developed to evolve the two-photon brightness of fluorescent proteins  commonly used in two-photon microscopy and neurological imaging studies. These brighter fluorescent proteins improve the imaging depth possible  with two-photon microscope systems. We also utilized our two-photon  imaging technique to image fluorescently stained latent fingermarks. We  found that two-photon imaging provides a significant advantage when  dealing with fingermarks on high contrast backgrounds like aluminum  cans. I was also a part of other nonlinear optics based research  projects including, two-photon spectroscopy, ultrafast photochemistry  characterization, and two-photon microscopy.

