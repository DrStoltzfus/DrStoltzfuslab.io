---
title: "Gerner Lab (UW): CytoMAP"
excerpt: "Multidimensional Analysis Pipeline for Histocytometry (CytoMAP)<br/> <a href='https://cstoltzfus.com/research/2019%20CytoMAP/'> <img src='/images/Logo.png'> </a>"
collection: research
---

**Sep. 2017 - July 2021**

[![Logo](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Logo.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Logo.png)

**CytoMAP** is a program that includes multiple advanced data analytic techniques for analyzing cell position and phenotype data. The goal is to take established analytical techniques, such as clustering algorithms, distance measurements, dimensional reduction, etc. and package them in a user friendly way, allowing researchers to explore complex cellular datasets.

CytoMAP seeks to fill the need for easy to use software facilitating spatial analysis of single cells. Thus, CytoMAP deals primarily with already segmented cells, i.e. tables of cells with position and channel values. My goal is to help users do something more with their segmented image data.

If you are interested in learning more please see the publication in Cell Reports.

[<img src="https://marlin-prod.literatumonline.com/cms/asset/atypon:cms:attachment:img:d51e6:rev:1588102390516-4191:pii:S2211124719X00170/cover.tif.jpg" style="zoom:50%;" />](https://www.cell.com/cell-reports/fulltext/S2211-1247(20)30423-X)

CytoMAP is publicly available as a [GitLab repo](https://gitlab.com/gernerlab/cytomap), as a [MATLAB app](https://www.mathworks.com/matlabcentral/fileexchange/75348-cytomap), or as a standalone [program](https://gitlab.com/gernerlab/cytomap/-/tree/master/StandaloneInstaller).

There is a mostly complete Wiki available [here.](https://gitlab.com/gernerlab/cytomap/-/wikis/home)

A few additional notes for the curious.

- CytoMAP works with tables of points exported from your chosen segmentation software
- All of the analysis supports 2D or 3D data
- CytoMAP supports analysis of large volumes, multiple samples, and many cell objects
- CytoMAP excels at defining regions and cellular patterns in whole slide or volume image data
- CytoMAP is UI based and does not require any coding knowledge

[![Overview](https://aws1.discourse-cdn.com/business4/uploads/imagej/optimized/3X/d/b/db54615296d14089d13fe254f6b0e0ebbef602c1_2_690x380.png)](https://aws1.discourse-cdn.com/business4/uploads/imagej/original/3X/d/b/db54615296d14089d13fe254f6b0e0ebbef602c1.png)



Try it out and turn your Biological questions into mathematical answers.

[![Example image of cells](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/CEA_CD11c_CD8_CD31.jpg)
