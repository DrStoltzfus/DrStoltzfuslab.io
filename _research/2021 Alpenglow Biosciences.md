---
title: "Alpenglow Biosciences 3D Spatial Biology"
excerpt: "Spatial Biology<br/> <a href='https://cstoltzfus.com/research/2021%20Alpenglow%20Biosciences/'> <img src='/images/AB_logo.png'> </a>"
collection: research
---

**July. 2021 - Present**

[![Logo](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/AB_logo.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/AB_logo.png)

I am currently the Director of Research and Development at Alpenglow Biosciences where we are developing scalable machine learning based image segmentation and analysis pipelines.
