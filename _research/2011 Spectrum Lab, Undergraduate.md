---
title: "Undergraduate Research; Spectrum Lab (MSU)"
excerpt: "Differential Absorption LIDAR (DIAL) <br/> <a href='https://cstoltzfus.com/research/2011%20Spectrum%20Lab,%20Undergraduate/'>     <img src='/images/DIAL.png'> </a>"
collection: research
---

**Jan. 2010 - Dec. 2011**

As an undergraduate at Montana State University I had the pleasure of working at Spectrum lab developing and characterizing a laser system for the remote sensing of chemicals, using differential absorption LIDAR (DIAL) in the mid-IR (3-4 um) range. I was responsible for characterizing and optimizing a Mono-Block Nd:YAG laser resonator (from Scientific Materials) and the associated optical parametric amplification system used to achieve a tunable pulsed mid-IR output. Spectrum lab gave me an excellent introduction to hands on research and it was here, aligning lasers, writing control software, and building optical systems, that my idle curiosity for lasers and how things work turned into a full-fledged passion. Presenting this research at the annual OpTeC conference introduced me to the local optical community and gave me a glimpse of what a career in science could be like.

[![OptTEC 2010](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/GgO59dWKozRcJJH-7H5kM9L0mNCF4Evq2EF4dLbQE-k.webp)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/GgO59dWKozRcJJH-7H5kM9L0mNCF4Evq2EF4dLbQE-k.webp "OpTEC 2010 Poster")



