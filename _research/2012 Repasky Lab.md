---
title: "Repasky Lab (MSU)"
excerpt: "Micro-Pulsed LIDAR <br/><a href='https://cstoltzfus.com/research/2012%20Repasky%20Lab/'><img src='/images/microLIDAR.png'></a><br/>Photo from E. Casey 2012"
collection: research
---

**Jan. - Aug. 2012**

In the few months between graduating and starting grad school I volunteered in [Dr. Kevin Repasky's lab](http://www.montana.edu/krepasky/) where I worked on the LabVIEW instrument control  and MATLAB data analysis software for a micro-pulsed LIDAR system. This system was used to measure aerosols for atmospheric studies. I worked with Erin Michelle Casey on automating the data acquisition using LabVIEW and improving the MATLAB  code, which analyzed the laser backscatter data. Parts of this effort were published in Erin's thesis available [here](https://scholarworks.montana.edu/xmlui/handle/1/1050).
