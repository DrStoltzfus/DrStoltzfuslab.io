---
title: "Spectrum Lab (MSU) and Advanced Microcavity Sensors"
excerpt: "Liquid Crystal, Tunable Micro-Optical Filters<br/><a href='https://cstoltzfus.com/research/2017%20Spectrum%20Lab%20(MSU)%20and%20Advanced%20Microcavity%20Sensors/'><img src='/images/LCAM.PNG'></a><br/>Figure from Stoltzfus et. al. 2017"
collection: research
---

**May 2016 - Aug. 2017**

From 2016-2017 I worked on a collaborative project with Advanced Microcavity Sensors LLC and MSU. I characterized and developed a high resolution, liquid  crystal, tunable micro-optical filter, as well as laser systems. These  systems use microscopic concave mirrors to form an optical cavity with  excellent spectral resolution. This project aimed to achieve a robust  and high performance-to-cost spectral system that can be integrated into existing microscope and other imaging technologies.

This research was published [here](https://doi.org/10.1364/OL.42.002090).

Aspects of this research were discussed by Fartash Vasefi at SPIE BiOS 2018 [here](https://www.spiedigitallibrary.org/conference-proceedings-of-spie/10497/2292620/Mie-scattering-characterization-by-ultra-spectral-illumination-using-micro-sized/10.1117/12.2292620.short?SSO=1).

