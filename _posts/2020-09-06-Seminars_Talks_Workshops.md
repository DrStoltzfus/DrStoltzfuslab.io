---
layout: single
title:  "Incomplete List of Talks and Seminars"
date: 2020-09-06
permalink: /posts/2020/Virtual-Talks/
categories: 
  - Virtual Seminars
tags:
  - Talks
  - Spatial Omics
  - Immunology
  - Physics Colloquia 
---
# Virtual Seminars

COVID-19 has had a major impact on all of our lives. Working remotely has forced many of us to change the way we do science, collaborate, and communicate our findings. One byproduct of this new environment has been virtual conferences, talks, and seminars that have become available. Below is a list of a few interesting ongoing talks/workshops/seminars that I have come across. 

If you know of other public virtual seminars about spatial imaging, physics, optics, immunology, or equity and inclusion in science let me know through twitter [@C_Stoltzfus](https://twitter.com/C_Stoltzfus) or email: calebst@gmail.com.




- [Global Immunotalks](https://labs.biology.ucsd.edu/zuniga/global_immunotalks.htm): Wednesdays 9:00 AM PDT

  - This seminar series hosts talks from immunologists covering a broad range of topics.

- [Spatial Omics](http://spatialomics.net/): Fridays 11:00 AM PDT
  
  - This ongoing seminar series hosts talks about the imaging, analysis, and technology associated with spatial analysis and imaging of cells in tissues. 

- [Virtual AMO Series](https://sites.google.com/stanford.edu/virtual-amo-seminar/home?authuser=0): Fridays 12:00 PM PDT

  - This ongoing seminar series hosts talks about Atomic, Molecular, and Optical physics (AMO).

  

  ---

- [Berkeley Physics Colloquia](https://physics.berkeley.edu/news-events/events/20200907/fall-2020-colloquia-schedule)

  - The fall colloquia hasn't started yet, but videos of past talks are publicly available.

  

  ## University of Washington 

  These talks are available for anyone with a University of Washington affiliation.

- [PR2ISM](https://www.pr2ism.org/)
	
	- This series of workshops and panel discussions cover broad topics related to diversity equity and inclusion in STEM. They offer amazing discussions of how to make higher education, mentorship, and research more equitable.
	
	
	
	
	
	---
	
	Updated Sep. 6 2020

