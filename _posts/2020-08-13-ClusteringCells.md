---
title: 'Phenotyping Single Cell Data in CytoMAP'
date: 2020-08-13
permalink: /posts/2020/08/CytoMAP Clustering Cells/
categories: 
  - CytoMAP
tags:
  - CytoMAP
  - Cellular Phenotyping
  - spatially resolved single cell analytics
---

In this post I walk through how to phenotype cells using CytoMAP
======

After preparing tissues, imaging them, and segmenting those images; figuring out what cell types you can define is the next step in the long journey of image analysis. The are many ways to do this and a plethora of software options out there. For many cases, if you have a few markers and a specific question about a specific population of cells, manually defining cells based on their mean fluorescent intensity (MFI) is the way to go. However, if you have a bunch of channels, and are asking more general questions about what is in the tissue microenvironment, clustering based cellular phenotyping can be very informative. In this post I am going to walk through how to do this in CytoMAP using the example data available [here](https://gitlab.com/gernerlab/exampledata/-/tree/master/Steady%20State%20Murine%20Lymph%20Node%2020um%20Myeloid%20pannel/Cell%20position%20and%20MFI%20csv%20data).



## Step 1: Import your data into CytoMAP

This might might sound like a trivial step, but if your data isn't formatted correctly, or your starting data has junk in it every step down stream from this will be much more difficult. To start, if you haven't defined any cell types, you should have a .csv file for each sample and the name of the file should match the name of the sample like this:

> LN1_auLN_All.csv
>
> LN2_auLN_All.csv
>
> LN3_auLN_All.csv
>
> LN4_bLN_All.csv
>
> LN5_bLN_All.csv

Here we have five steady state mouse Lymph Nodes (LN) that are either auricular (auLN) or brachial (bLN). Inside the .csv files each row is a single cell and each column is a different channel:

| CD64_PE | SIRPa_594 | CD207_488 | CD169_514 | Lyvev1_490LS | CD11c_421 | CD31_480 | MHC2_395xl | Clec9a_633 | CD301b_660 | B220_700 | CD3_APC-F750 | CD301b_corrected | SIRPa_corrected | CD31_corrected | CD11c_corrected | CD169_corrected | MHC2_B220clean | Myeloid | PositionX | PositionY | PositionZ | chID | Sphericity | Volume  | Classifier |
| ------- | --------- | --------- | --------- | ------------ | --------- | -------- | ---------- | ---------- | ---------- | -------- | ------------ | ---------------- | --------------- | -------------- | --------------- | --------------- | -------------- | ------- | --------- | --------- | --------- | ---- | ---------- | ------- | ---------- |
| 1.35445 | 0.562728  | 94.5591   | 18.9269   | 37.4549      | 0.651035  | 3.16626  | 2.84044    | 5.75457    | 17.3118    | 1.93057  | 1.30694      | 15.9635          | 0.501827        | 3.12424        | 0.613276        | 11.7058         | 2.32278        | 80.7795 | 382.9     | 778.6     | 9.23048   | 5594 | 0.887945   | 241.933 | 1          |
| 7.20514 | 28.9541   | 26.5623   | 14.0632   | 12.3248      | 20.1613   | 15.9078  | 19.6887    | 11.4882    | 28.5522    | 49.3559  | 19.0611      | 8.37195          | 20.7848         | 10.9094        | 18.1807         | 10.3939         | 7.1032         | 67.0661 | 739.4     | 688.4     | 9.06742   | 5595 | 0.648447   | 334.402 | 1          |
| 16.7272 | 52.9096   | 3.59773   | 13.2047   | 34.0698      | 33.8806   | 31.0185  | 29.1475    | 9.86759    | 13.1412    | 36.6625  | 25.7789      | 4.42623          | 33.5305         | 20.1753        | 29.3018         | 7.93401         | 11.1055        | 69.5939 | 862.2     | 715.8     | 10.51935  | 5596 | 0.809646   | 342.535 | 1          |
| 12.0737 | 44.4457   | 2.28177   | 5.61142   | 22.2578      | 46.7551   | 20.4236  | 20.1952    | 15.6114    | 10.6648    | 36.0534  | 26.1142      | 3.45488          | 32.7035         | 9.62431        | 43.6059         | 4.44383         | 5.6372         | 76.8895 | 958.1     | 777.3     | 11.38515  | 5597 | 0.842428   | 75.3365 | 1          |
| 26.4615 | 40.5275   | 3.46055   | 25.3826   | 186.983      | 16.7826   | 20.345   | 32.3862    | 9.05229    | 4.70183    | 50.5826  | 17.4073      | 0.588991         | 31.1642         | 14.845         | 15.0257         | 19.8404         | 10.5           | 60.7404 | 979.8     | 784.1     | 9.72825   | 5598 | 0.607925   | 148.746 | 1          |
| 19.4325 | 68.9427   | 2.73145   | 8.77993   | 14.6998      | 21.7487   | 21.4009  | 20.8259    | 7.29975    | 10.5274    | 44.8794  | 20.4414      | 1.95953          | 53.067          | 14.5974        | 18.9553         | 6.10919         | 4.65641        | 67.336  | 1066.2    | 685.2     | 10.10915  | 5599 | 0.798238   | 340.653 | 1          |
| 10.934  | 62.8727   | 4.90024   | 11.6536   | 33.1681      | 12.6167   | 30.6119  | 26.5805    | 14.2522    | 15.304     | 65.1987  | 20.8924      | 2.022            | 42.4509         | 25.1736        | 9.52474         | 6.23959         | 4.34171        | 56.6489 | 1086.4    | 733.4     | 10.78305  | 5600 | 0.73209    | 172.278 | 1          |
| 7.73832 | 27.6059   | 2.43146   | 6.7134    | 12.2555      | 41.0374   | 26.0826  | 20.081     | 6.42835    | 14.2866    | 47.785   | 16.7352      | 2.77726          | 16.4143         | 13.595         | 36.9143         | 4.72274         | 3.56854        | 53.9673 | 1095.9    | 737.6     | 11.66165  | 5601 | 0.688186   | 84.8298 | 1          |
| 9.52778 | 10.3693   | 30.7819   | 18.2582   | 10.106       | 18.5597   | 16.6615  | 23.8086    | 15.93      | 14.8097    | 46.9403  | 16.6543      | 3.26646          | 6.50617         | 11.6811        | 16.5514         | 13.7119         | 5.57819        | 60.4424 | 1147.4    | 780.8     | 11.09335  | 5602 | 0.807941   | 133.695 | 1          |

For best results, try not to use symbols, start a name with a number, or have spaces in the channel names. CytoMAP is fairly robust to these channel names, but sometimes this still causes errors. If you want to re-name the headers of a bunch of .csv files to make them consistent across samples, I have a MATLAB function to help [here.](https://gitlab.com/gernerlab/imarisxt_histocytometry/-/blob/master/Basic_Helpers/combine_and_rename_csv.m) If you name your positional columns PositionX, PositionY, PositionZ, CytoMAP will automatically recognize these as the spatial position channels. If CytoMAP can't find position it will ask when loading data. Additionally, since everything in CytoMAP is set up to work with three-dimensional data, if your data just has X, and Y we will add a Z channel that is all zeros. Now that your data is formatted correctly simply run *File* > *Import Multiple Samples*

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_load_select.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_load_select.png)

After selecting the samples you want to load a window will pop up where you can rename any samples or channels. These can also be changed later in CytoMAP so for now we are going to just click *Load*.

[![Re-Name Channels](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_load_rename.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_load_rename.png)

The last step is to make sure everything worked. To do this make a *New Figure*, plot some of your samples, and color code them by a few different channel markers. Below are the five LNs I loaded in color coded by B220, Cell Density, Lyve1, CD169, and CD3. All of the channel intensities look similar to what I saw in the original images.

[![Check Loaded Cells](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_load_plot.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_load_plot.png)

## Step 1.5: Pre-gating

Sometimes it is useful to build surfaces on different objects in your image then load all of your surfaces into one .csv. You can then add a "classifier" channel that keeps track of which object type each cell is. In the example data used here we have done just that. We built surfaces on Myeloid cells then added spot objects. The spot objects give us locally averaged pixel values which we can use to identify the T cell zone, and B cell follicles in our image without explicitly segmenting on T cells and B cells. To simplify this example we are only going to cluster the Myeloid cell population. To get to just the Myeloid cells, make a *New Figure*, plot Classifier on the Y axis and Sphericity on the X axis. Sine all of the spot objects have Sphericity = 1, using this channel simplifies knowing which objects are spots and which are Myeloid cells. 

To gate on these cells click the *new rectangular gate* button at the top of the figure window (left red arrow). Name your new population, click ok, then draw your gate on the the plot. Finally click the save gate button (right red arrow).

[![Gate On Cells](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Gate_drawgate.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Gate_drawgate.png)

Next we can plot our two new populations of cells to see their spatial distribution below. For this lymph node the Myeloid cells aren't located in the B cell follicles.

[![Cellular Spatial Distribution](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Gate_plot.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Gate_plot.png)

## Step 2: Run the clustering algorithm

Now that we have isolated just our Myeloid cells we can cluster these cells into different phenotypes. To do this click the *Cluster Cells* button in the main CytoMAP window. This will bring up a new window with a lot of options. For this example, we want to use all of our fluorescent channels but we don't want to double any channels. Thus, for the few channels where we have a corrected version (ran background subtraction etc.) we want to be sure to only use the corrected version and not the raw version of that channel. In the bottom left of this window we selected standardized cellular MFI and we did this calculation on a sample by sample basis and not for the whole pooled dataset. We manually selected 30 clusters so that we over-cluster the data. Picking a number of regions is faster than letting the computer decide and we can always combine clusters that are too similar later.

[![Cluster Cells User Interface](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Cluster_Options.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Cluster_Options.png)



After running the clustering algorithm we can now color-code our cells by which cluster number they belong to. This highlights the distinct spatial regions the individual cell types live in.

[![Cell Cluster Spatial Distribution](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Cluster_plot.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Cluster_plot.png)

After checking the spatial distribution of the clusters we can use the *cell_heatmaps* extension to look at the average channel intensity in the cells that belong to each of the clusters we just defined. 

[![Cell MFI Heatmaps](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Cluster_Heatmap.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Cluster_Heatmap.png)

## Step 3: Annotate the cell clusters

The final step in defining cells with CytoMAP is to annotate the different clusters of cells with names that make sense. To do this make plots of the spatial distribution of your cells, plots of the channel MFI per cluster, and plots of the different channel MFI per cell. Next use the *annotate cells* function to name your populations.

[![Annotate Plots](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Annotate_Plots.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Annotate_Plots.png)

By clicking on your plots of cells color-coded by cluster number, you can explore which cell type belongs to each cluster. In the example below we see that cells from cluster number 24 has high CD64, and CD169 expression. In the Annotate Cells window we can name this cluster subcapsular sinus macrophages (SCS Macs).

[![Cell MFI Heatmaps](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Annotate_PlotExploration.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Annotate_PlotExploration.png)

After we have given a name to all of the cell types we are interested in we can plot these cells in space, but now color coded by the annotated cell type. 

[![Cell MFI Heatmaps](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Annotate_CellPlots.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_Annotate_CellPlots.png)

We are now ready to use these cell types for downstream analysis such as cell-cell correlation, cell distance relationships, or neighborhood based region analysis. For more information, see my [CytoMAP Wiki page](https://gitlab.com/gernerlab/cytomap/-/wikis/home). If you get stuck, or need some help; post a topic on the [image.sc](https://forum.image.sc/new) forum with the tag *cytomap*. 

Best of luck exploring your data!

