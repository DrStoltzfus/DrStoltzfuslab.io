---
title: 'CytoMAP Questions and Comments'
date: 2020-07-09
permalink: /posts/2020/07/08/CytoMAP-Questions
tags:
  - CytoMAP
  - user feedback
  - image analysis forum
---

# Use the image.sc forum

Since the public release of CytoMAP, I have had a handful of users ask me questions using email, LinkedIn, Twitter, and other methods. As I am sure every new software developer discovers at some point, this is a wildly inefficient way to deal with user feedback. To remedy this I am recommending people use the image analysis forum at [forum.image.sc](https://forum.image.sc) to ask questions about CytoMAP's functionality, or bugs they may find in the code. 

**To ask a question, create a new topic and include the *cytomap* tag.**

Thank you to everyone who has been using the software and suggesting new features and ways of analyzing spatially resolved single cell data!