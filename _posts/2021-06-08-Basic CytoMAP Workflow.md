---
title: 'Simplified CytoMAP Workflow'
date: 2021-06-08
permalink: /posts/2021/06/CytoMAP Demo/
categories: 
  - CytoMAP
tags:
  - CytoMAP
  - Cellular Phenotyping
  - Region Definitions
  - spatially resolved single cell analytics
---



How to phenotype cells and define regions using CytoMAP
======

**After** preparing tissues, imaging them, and **segmenting** those images; figuring out what cell types you can define and answering biological questions is the next step in the long journey of image analysis. The are many ways to do this and a plethora of software options out there. For many cases, if you have a few markers and a specific question about a specific population of cells, manually defining cells based on their mean fluorescent intensity (MFI) is the way to go. However, if you have a many channels, and are asking in depth questions about tissue microenvironments, clustering based analysis methods are very informative. In this workshop I am going to walk through how to do this in CytoMAP using a simplified example dataset derived from 5 steady state mouse lymph nodes stained with a myeloid cell focused antibody panel. 

full dataset available here: https://gitlab.com/gernerlab/exampledata/-/tree/master/Steady%20State%20Murine%20Lymph%20Node%2020um%20Myeloid%20pannel/Cell%20position%20and%20MFI%20csv%20data



## Step 0: Installing CytoMAP

Download the compiled executable instillation file for CytoMAP here: https://gitlab.com/gernerlab/cytomap/-/blob/master/StandaloneInstaller/CytoMAP_Installer_WindowsV1.4.20.exe



Read about other install options here: https://gitlab.com/gernerlab/cytomap/-/wikis/Installation-Guide

## Step 1: Import your data into CytoMAP

This might might sound like a trivial step, but if your data isn't cleaned and formatted correctly every step down stream from this will be much more difficult. To start, if you haven't defined any cell types, you should have a .csv file for each sample and the name of the file should match the name of the sample like this:

> LN1_auLN_All.csv
>
> LN2_auLN_All.csv
>
> LN3_bLN_All.csv
>
> LN4_bLN_All.csv

Here we have four steady state mouse Lymph Nodes (LN) that are either auricular (auLN) or brachial (bLN). Inside the .csv files each row is a single cell and each column is a different channel:

| CD64_PE | SIRPa_594 | CD207_488 | CD169_514 | Lyvev1_490LS | CD11c_421 | CD31_480 | MHC2_395xl | Clec9a_633 | CD301b_660 | B220_700 | CD3_APC-F750 | X      | Y     | Z        | chID | Sphericity | Volume  |
| ------- | --------- | --------- | --------- | ------------ | --------- | -------- | ---------- | ---------- | ---------- | -------- | ------------ | ------ | ----- | -------- | ---- | ---------- | ------- |
| 1.35445 | 0.562728  | 94.5591   | 18.9269   | 37.4549      | 0.651035  | 3.16626  | 2.84044    | 5.75457    | 17.3118    | 1.93057  | 1.30694      | 382.9  | 778.6 | 9.23048  | 5594 | 0.887945   | 241.933 |
| 7.20514 | 28.9541   | 26.5623   | 14.0632   | 12.3248      | 20.1613   | 15.9078  | 19.6887    | 11.4882    | 28.5522    | 49.3559  | 19.0611      | 739.4  | 688.4 | 9.06742  | 5595 | 0.648447   | 334.402 |
| 16.7272 | 52.9096   | 3.59773   | 13.2047   | 34.0698      | 33.8806   | 31.0185  | 29.1475    | 9.86759    | 13.1412    | 36.6625  | 25.7789      | 862.2  | 715.8 | 10.51935 | 5596 | 0.809646   | 342.535 |
| 12.0737 | 44.4457   | 2.28177   | 5.61142   | 22.2578      | 46.7551   | 20.4236  | 20.1952    | 15.6114    | 10.6648    | 36.0534  | 26.1142      | 958.1  | 777.3 | 11.38515 | 5597 | 0.842428   | 75.3365 |
| 26.4615 | 40.5275   | 3.46055   | 25.3826   | 186.983      | 16.7826   | 20.345   | 32.3862    | 9.05229    | 4.70183    | 50.5826  | 17.4073      | 979.8  | 784.1 | 9.72825  | 5598 | 0.607925   | 148.746 |
| 19.4325 | 68.9427   | 2.73145   | 8.77993   | 14.6998      | 21.7487   | 21.4009  | 20.8259    | 7.29975    | 10.5274    | 44.8794  | 20.4414      | 1066.2 | 685.2 | 10.10915 | 5599 | 0.798238   | 340.653 |
| 10.934  | 62.8727   | 4.90024   | 11.6536   | 33.1681      | 12.6167   | 30.6119  | 26.5805    | 14.2522    | 15.304     | 65.1987  | 20.8924      | 1086.4 | 733.4 | 10.78305 | 5600 | 0.73209    | 172.278 |
| 7.73832 | 27.6059   | 2.43146   | 6.7134    | 12.2555      | 41.0374   | 26.0826  | 20.081     | 6.42835    | 14.2866    | 47.785   | 16.7352      | 1095.9 | 737.6 | 11.66165 | 5601 | 0.688186   | 84.8298 |
| 9.52778 | 10.3693   | 30.7819   | 18.2582   | 10.106       | 18.5597   | 16.6615  | 23.8086    | 15.93      | 14.8097    | 46.9403  | 16.6543      | 1147.4 | 780.8 | 11.09335 | 5602 | 0.807941   | 133.695 |

For best results, try not to use symbols, start a name with a number, or have spaces in the channel names. CytoMAP is fairly robust to these channel names, but sometimes this still causes errors. If you need to re-name the headers of a bunch of .csv files to make them consistent across samples, I have a MATLAB function to help [here.](https://gitlab.com/gernerlab/imarisxt_histocytometry/-/blob/master/Basic_Helpers/combine_and_rename_csv.m) If you name your positional columns X, Y, Z, CytoMAP will automatically recognize these as the spatial position channels. If CytoMAP can't find position it will ask when loading data. Additionally, since everything in CytoMAP is set up to work with three-dimensional data, if your data just has X, and Y it will add a Z channel that is all zeros. Now that your data is formatted correctly simply run *File* > *Import Multiple Samples*

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_load_select.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/PhenotypingCells_load_select.png)

After selecting the samples you want to load a window will pop up where you can rename any samples or channels. These can also be changed later in CytoMAP so for now we are going to just click *Load*.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img01_Load.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img01_Load.png)

The last step is to make sure everything worked. To do this make a *New Figure*, plot some of your samples, and color code them by a few different channel markers. Below are the LNs I loaded in color coded by Lyve1. All of the channel intensities look similar to the original images.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img02_check.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img02_check.png)

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img03_confocal.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img03_confocal.png)

## Step 2: Phenotype Cells

To define cell types you can **Manually gate** on marker expression to define cell types

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img04_gates.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img04_gates.png)

and/or **Cluster** cells into different phenotypes. For this walkthrough we will cluster cells. To do this click the *Cluster Cells* button in the main CytoMAP window. This will bring up a new window with a lot of options. For this example, we want to use all of our fluorescent channels expressed by myeloid cells and not the position of the cells. In the bottom left of this window we selected standardized cellular MFI and we did this calculation on a sample by sample basis and not for the whole pooled dataset. We manually selected 5 clusters.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img05_clusterinterface.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img05_clusterinterface.png)

After running the clustering algorithm we can now color-code our cells by which cluster number they belong to. This highlights the distinct spatial regions the individual cell types live in.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img06_cellclusters.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img06_cellclusters.png)

After checking the spatial distribution of the clusters we can use the *Region Statistics* interface to look at the average channel intensity in the cells that belong to each of the clusters we defined. 

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img08_cellstats.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img08_cellstats.png)

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img09_cellstatsresults.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img09_cellstatsresults.png)

We could also run dimensionality reduction and color code the resulting plots by cluster and MFI of the cells to visualize how the clusters are defined.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img10_tsne.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img10_tsne.png)



## Step 3: Annotate the cell clusters

The final step in defining cells with CytoMAP is to annotate the different clusters of cells with names and convert them from clusters to cell phenotypes. To do this use plots of the spatial distribution of your cells and plots of the channel MFI per cluster. Next use the *annotate cells* function to name your populations.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img11_annotate.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img11_annotate.png)

By clicking on plots of cells color-coded by cluster number, you can explore the clusters. In the example below we see that cells from cluster number 5 has high CD64, and CD169 expression and is located peripherally in the lymph node. In the Annotate Cells window we can name this cluster macrophages.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img12_checkannotate.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img12_checkannotate.png)

We are now ready to use these cell types for downstream analysis such as cell-cell correlation, cell distance relationships, or neighborhood based region analysis. 



## Step 4: Define Neighborhoods

Now that we have cell types we can calculate the number of cells per neighborhood using the *Define Neighborhoods* interface. For this example we are defining 50 um Raster Scanned Neighborhoods. This option will even distribute the neighborhoods throughout the sample space and calculate the number of cells of each type within 50 um of each neighborhood's position. 

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img13_RSNdef.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img13_RSNdef.png)

## Step 5: Cluster Neighborhoods into Regions

Next we can use the same clustering algorithms we used to define cells to cluster neighborhoods into regions in the tissue. When clustering regions we want to use the cell types we defined above, as well as any MFI that are not represented in our cell types. We don't include the position of the neighborhoods meaning the clustering algorithm will group similar neighborhoods together regardless of where they are in the tissue.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img14_clusterRSN.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img14_clusterRSN.png)

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img15_regions.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img15_regions.png)

It is important to note at this point that the more detail you feed into CytoMAP the more informative the regions will be. For example;  In Stoltzfus et. al. 2020 we defined more Myeloid cell subtypes and included B cell and T cell objects from these lymph nodes. This resulted in more informative definitions of the regions.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img16_realregs.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img16_realregs.png)

## Step 6: Region Statistics 

Now that we have defined regions we can explore how the regions are defined, what regions are present in the samples and how these regions change across tissue groups. We can use the *Region Statistics* interface to plot the Fold Change in the number of cells per region as well as the percentage of neighborhoods of each region type.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img17_regstats.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img17_regstats.png)

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img18_regstatsresults.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Basic_Workflow/img18_regstatsresults.png)



This is one of many possible workflows using CytoMAP. For more information on the functions you can use to explore your data see the CytoMAP Wiki page:

[https://gitlab.com/gernerlab/cytomap/-/wikis/home](https://gitlab.com/gernerlab/cytomap/-/wikis/home)



If you get stuck, or need some help post a topic on the image analysis forum with the *cytomap* tag:

[https://forum.image.sc](https://forum.image.sc)
