---
title: 'Importing Imaris Object Statistics into CytoMAP'
date: 2021-04-20
permalink: /posts/2021/04/CytoMAP Imaris Objects/
categories: 
  - CytoMAP
tags:
  - CytoMAP
  - Cellular Phenotyping
  - Imaris
  - Data Formats
---

In this post I walk through how to export surface object statistics from Imaris and import them into CytoMAP
======

In this post I walk through how to export data from the Imaris image analysis software, re-format that data, then import the data into CytoMAP. In this post I am using the example data available [here](https://gitlab.com/gernerlab/exampledata/-/tree/master/Steady%20State%20Murine%20Lymph%20Node%2020um%20Myeloid%20pannel/Cell%20position%20and%20MFI%20csv%20data).

## Step 1: Export Statistics

The first step in this process is to export the desired statistics of the objects you have created in Imaris into .csv files. To do this click on the object, then click the statistics tab, then click the *Export All Statistics to File* button in the bottom of the interface.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide2.png)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide2.png)

Exporting data from Imaris (as of Imaris version 9.3.0 ) will result in a folder with a single .csv file for each statistic you have selected. You can change which statistics Imaris exports in the settings. I typically use Mean Fluorescence intensity, sphericity, ID, Volume, and of course position. In each of these .csv files there will be a column with the actual values you want and some other columns with things like channel, image name etc. This will name each channel with the channel number and not the channel name.

To use this data we want to re-format all these individual .csv files into one .csv file with all the statistics we exported in a single column for each statistic and ideally all surface objects combined into one .csv file for each sample.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide4.PNG)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide4.PNG)

## Step 2: Reformat the .csv files

There are many ways to re-format our data. I am going to use a MATLAB extension I wrote that makes this easy called [Imaris_To_FlowJo_CSV_Converter_V6]( https://gitlab.com/gernerlab/imarisxt_histocytometry/-/tree/master/Basic_Helpers). I have a standalone version of this that you don't need MATLAB to run, an app version, or the raw code. In the figure below I have opened the code in MATLAB and clicked run. This initialized the window on the right of this figure. In this window click the File option then *Import Single Surface Object*.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide5.PNG)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide5.PNG)

In the resulting navigation window shown below, select **all** of the channels you want to concatenate into a single .csv file. This will ignore the Overall .csv file if you select it. After these have loaded the Converter interface should have a list of all of the statistics you selected as shown in the right window in the figure below. In this interface you can change the names of any of the channels. You can also add a sphericity column with all values equal to 1 with the bottom right button which is turned on by default. 

You can add a Classifier column to your data as well with any value you input in the box next to the classifier button. This is useful if you end up combining multiple objects from Imaris into one csv. You can have all your different surface objects in one file with Classifier = 1 for T cells, and Classifier = 2 for myeloid cells etc. 

Once you have named your channels click the *Save* button in the bottom left of the interface to save your combined .csv file. 

Alternatively, if you already have a .csv file with statistics from another object you can click the *Append* button which will add the current statistics to the bottom of any user selected .csv file. Be careful when doing this as this does not keep track of what order your channels are in or how many channels you have. If you append to the statistics file of an object with different channels, this will stick the data to the bottom of the file without any header separating the two data resulting in a mess.

For best results, try not to use symbols, start a name with a number, or have spaces in the channel names. If you name your positional columns PositionX, PositionY, PositionZ, CytoMAP will automatically recognize these as the spatial position channels. If CytoMAP can't find position it will ask when loading data. Additionally, since everything in CytoMAP is set up to work with three-dimensional data, if your data just has X, and Y we will add a Z channel that is all zeros. 

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide6.PNG)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide6.PNG)

After saving you should now have a new .csv file that has a single channel for each statistic you selected. There should also be a Classifier channel if you added one.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide7.PNG)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide7.PNG)

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide8.PNG)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide8.PNG)



## Step 3: Import into CytoMAP

Now that your data is formatted correctly, open CytoMAP and run *File* > *Load Table of Cells* and select your new combined .csv.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide9.PNG)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide9.PNG)

Once you have navigated through the load data wizard you can compare the positions of your imported objects to the surface objects in Imaris. The positions of the objects should be identical.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide11.PNG)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide11.PNG)

You can perform further checks such as gating on objects that are high in specific channels and comparing the position of those objects to the position of the cells that are high in that channel in the original image. Below I performed a quick check by gating on cells that express CD207, which is in the upper portion of this example lymph node in my original image (green dots in the figure at the top of this page) and my imported data.

[![Load Multiple Samples](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide12.PNG)](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/Imaris_Export_Img/Slide12.PNG)

For more information, see my [CytoMAP Wiki page](https://gitlab.com/gernerlab/cytomap/-/wikis/home). If you get stuck, or need some help; post a topic on the [image.sc](https://forum.image.sc/new) forum with the tag *cytomap*. 

Best of luck exploring your data!

