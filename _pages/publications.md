---
layout: archive
title: "Publications"
permalink: /publications/
author_profile: true
---

A full list of my publications is also available on [my Google Scholar profile.](https://scholar.google.com/citations?user=XlnugAMAAAAJ&hl=en)

{% for post in site.publications reversed %}
  {% include archive-single-pubs.html %}
{% endfor %}