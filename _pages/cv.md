---
layout: archive
title: "CV"
permalink: /cv/
author_profile: true
redirect_from:
  - /resume
---

{% include base_path %}

## Summary:

- PhD in physics; specializing in nonlinear optics, spectroscopy, imaging, and computational analysis
- 10+ years of optics and photonics experience
- 10+ years of LabVIEW, MATLAB, Mathematica, and other computational experience
- Quantitative image acquisition and analysis, LIDAR and DIAL systems, fluorescence microscopy, nonlinear optics, ultrafast spectroscopy, optimizing and characterizing laser systems, numerical modeling, instrument automation, liquid crystal devices, prototype development and characterization, tunable spectral filters, microscopic optical devices
- I am passionate about science, laser based technology, and pushing the limits of optical technologies

## Education

**2016** PhD in Physics, from Montana State University

**2014** M.S. in Physics, from Montana State University

**2011** B.S. in Physics, Minor in Mathematics, from Montana State University

## Employment History

### Professional

**2021-Present** Director of Research and Development, Aplenglow Biosciences

**2017** Research Scientist, Advanced Microcavity Sensors

### Academic

**2017-2021** Washington Research Foundation Postdoctoral Fellow, Gerner Lab, University of Washington (UW) 

**2016-2017** Research Scientist, Spectrum Lab, Montana State University (MSU)

**2016** Teaching Assistant, Introductory Physics, and Advanced Optics (MSU)   

**2012-2016** Graduate Research Assistant, Rebane lab (MSU)  

**2012** Research Assistant, Repasky lab (MSU)

**2010-2011** Undergraduate Research Assistant, Spectrum Lab (MSU)  

**2010** Teaching Assistant, Holography Lab (MSU)

### Volunteer

**2021-Present** Washington Research Foundation Postdoctoral Fellowship Selection Committee

**2004-2005** Board Member, Project Manager, and Event Coordinator, Bozeman Youth Initiative (non-profit)

## Experience

### Programing

MATLAB/Mathematica/Python - image analysis, numerical modeling, object oriented programing, data analysis, and machine learning

LabVIEW - instrument automation and data acquisition

Other; ImageJ/Fiji, Origin, Excel, Word, PowerPoint, Imaris, Aivia, leica LASX, FlowJo, Prism, 3D modeling software (Autodesk)

### Optical Devices / Lab Skills

I have experience using HeNe, DFB, DPSS, high power lasers, pulsed, and femtosecond lasers, regenerative amplifiers (including alignment and initial setup / construction of these systems), laser cooling systems, beam splitters, mirrors, attenuators, interferometers, diffractive optical elements, monochromators, spectrometers, optical spectrum analyzers, nonlinear crystals, acousto-optic modulators, optical parametric amplifiers, optical fiber systems, ellipsometers, autocorrelators, digital multimeters, oscilloscopes, DACs, high voltage power supplies, photodetectors, CCD cameras (both cooled and room temperature), microscopes (two-photon, confocal, and other custom systems), and other devices related to assembling and characterizing optical systems.

### Educational

I have excelled in courses covering many topics such as nonlinear optics, Fourier optics, statistical mechanics, classical mechanics (orbital mechanics), quantum field theory, numerical computation, mathematical methods, quantum mechanics, laser physics, advanced optics, holography, laboratory electronics, and laboratory methods.

## Professional Development and Research

### Select Conference Presentations

**2019** Hallmarks of Cancer, Seattle WA, USA. _Utilizing Machine Learning and Quantitative Imaging to Explore Tumor Microenvironments_

**2019** BioImage Informatics, Seattle WA, USA. _Quantification and visualization of tissue microenvironments and cellular interactions by clustering neighborhoods using CytoMAP_

**2018** World Molecular Imaging Congress, Seattle WA, USA. _Utilizing Machine Learning and Quantitative Imaging to Visualize Tissue Microenvironments_

**2015** HBSM, Tartu Estonia. _Wide field of view, broadly wavelength-tunable two-photon imaging; principles and applications_

**2014** SPIE Photonics West, San Francisco CA, USA. _A multidimensional screening method for the selection of two-photon fluorescent proteins_

**2013** SPIE Photonics West, San Francisco CA, USA. _Multiphoton bleaching of red fluorescent proteins_

### Honors and Awards

**2019-2021** [Washington Research Foundation Postdoctoral Fellowship](http://wrfseattle.org/fellows.php)

**2011** [Graduated with Honors](https://physics.montana.edu/news/awards2012.html), Montana State University

**2010-2011** [USP Scholarship](https://www.montana.edu/usp/archives/ay2010-2011.html), Montana State University

### Select Publications

-  Stoltzfus, Caleb R., Jakub Filipek, Benjamin H. Gern, Brandy E. Olin,  Joseph M. Leal, Yajun Wu, Miranda R. Lyons-Cohen et al. *CytoMAP: a  spatial analysis toolbox reveals features of myeloid cell organization  in lymphoid tissues.* Cell reports **31**, no. 3 107523 (2020)
- C. R. Stoltzfus and A. Rebane, _Optimizing ultrafast illumination for multiphoton-excited fluorescence imaging_, Biomed. Opt. Express **7** , 1768-1782, (2016).
- C. R. Stoltzfus, L. M. Barnett, G. Wicks, A. Mikhaylov, T. E. Hughes, and A. Rebane, _Two-photon directed evolution of green fluorescent proteins_, Sci. Rep. **5** , 11968 (2015).

### Research

**[Gerner Lab](https://cstoltzfus.com/research/2019%20CytoMAP/)**

I worked on an interdisciplinary research project in Michael Gerner&#39;s lab at the University of Washington, attempting to elucidate how the spatial organization of immune cells in tumor microenvironments influence disease progression and treatment outcomes. Specifically, I used multi-dimensional spectrally resolved confocal microscopy to probe the local environments of immune cells. This research involved imaging large tissue sections and developing software tools to interrogate the cellular composition, global tissue architecture, and location and type of cell-cell interactions.

**[Spectrum Lab (MSU) and Advanced Microcavity Sensors](https://cstoltzfus.com/research/2017%20Spectrum%20Lab%20(MSU)%20and%20Advanced%20Microcavity%20Sensors%20-%20Copy/)**

I worked on a collaborative project with Advanced Microcavity Sensors LLC and MSU. I characterized and developed a high resolution, liquid crystal, tunable micro-optical filter, as well as laser systems. These systems use microscopic concave mirrors to form an optical cavity with excellent spectral resolution. This project aimed to achieve a robust and high performance-to-cost spectral system that can be integrated into existing microscope and other imaging technologies.

**[Rebane Lab](https://cstoltzfus.com/research/2016%20Rebane%20Lab/)**

In Dr. Aleksander Rebane&#39;s lab I co-invented and optimized a wide field-of-view two-photon imaging system capable of capturing the two-photon excited fluorescence of ~10 cm-sized samples. We used this new imaging system combined with automated image analysis techniques I developed to evolve the two-photon brightness of fluorescent proteins commonly used in two-photon microscopy and neurological imaging studies. These brighter fluorescent proteins improve the imaging depth possible with two-photon microscope systems. We also utilized our two-photon imaging technique to image fluorescently stained latent fingermarks. We found that two-photon imaging provides a significant advantage when dealing with fingermarks on high contrast backgrounds like aluminum cans. I was also a part of other nonlinear optics based research projects including, two-photon spectroscopy, ultrafast photochemistry characterization, and two-photon microscopy.

**[Repasky Lab](https://cstoltzfus.com/research/2012%20Repasky%20Lab/)**

In Dr. Kevin Repasky&#39;s lab I worked on the LabVIEW instrument control and MATLAB data analysis software for a micro-pulsed LIDAR system. This system was used to measure aerosols for atmospheric studies. I worked on automating the data acquisition using LabVIEW and improving the MATLAB code, which analyzed the laser backscatter data.

**[Spectrum Lab](https://cstoltzfus.com/research/2011%20Spectrum%20Lab,%20Undergraduate/)**

My undergraduate research consisted of advancing a laser system for remote sensing of chemicals, using differential absorption LIDAR (DIAL) in the mid-IR (3-4 um range). I was responsible for characterizing and optimizing a Mono-Block Nd:YAG laser resonator (from Scientific Materials) and the associated optical parametric amplification system used to achieve a tunable pulsed NIR output.

### All Publications
  <ul>{% for post in site.publications reversed %}
    {% include archive-single-cv.html %}
  {% endfor %}</ul>


### All Talks
  <ul>{% for post in site.talks reversed %}
    {% include archive-single-talk-cv.html %}
  {% endfor %}</ul>


### All Teaching
  <ul>{% for post in site.teaching reversed %}
    {% include archive-single-cv.html %}
  {% endfor %}</ul>

