---
permalink: /
title: 
excerpt: "About me"
author_profile: true
redirect_from: 
  - /about/
  - /about.html
---

![CytoMAP, By the Gerner Lab](https://gitlab.com/DrStoltzfus/DrStoltzfus.gitlab.io/-/raw/master/images/banner.jpg "Vasculature of a naive mouse Lymph node")

I am currently the Director of Research and Development at Alpenglow Biosciences where me and my team are developing scalable machine learning based image segmentation and analysis pipelines.

At my core I am an experimentalist, I love trying to figure out how things work and if I can make them better. This has inspired my research across diverse fields such as quantitative image analysis at the [Department of Immunology](https://www.immunology.washington.edu/) within the University of Washington, School of Medicine,  nonlinear optics and fluorescent protein evolution in the [Physics department](http://www.physics.montana.edu/) at Montana State University, building microscopic optical filters at [Spectrum Lab](http://www.spectrum.montana.edu/), and to developing new ways of understanding how immune cells interact and organize in tissues. I am particularly interested in pushing the limits of optical technologies, like lasers and microscopes, and I am working on turning this hobby into a career. In 2016 I graduated with a PhD in Physics from Montana State University, where I spent my time playing with lasers and building imaging systems in the Rebane lab. I look forward to seeing what breakthroughs the future of science holds, and hope that I can be a part of them.

### How do you describe your research to colleagues?

I am working at [Alpenglow Biosciences](https://www.alpenglowbiosciences.com/) on a multitude of projects. Across all of our projects we are exploring how the spatial organization of cells and structures in tissue microenvironments influence disease progression and treatment outcomes. We use open-top light-sheet microscopy to image whole tissues or biopsies and large numbers of samples. From these images we quantify the local cellular environments across global tisue scales using custom machine learning approaches and spatial analysis with [CytoMAP](https://gitlab.com/gernerlab/cytomap), which allows us to extract information from our large imaging  datasets and interrogate cellular composition, tissue architecture, and  locations of cell-cell interactions.
		
### How do you describe your current research to non-scientists?

I am trying to create 3D schematics of tissues that describe where all of the cells are, how they are interconnected, and how their organization changes in different diseases. In addition to the numbers and types of cells in tissues, the position of cells within tissues affects both how individual cells interact, and how whole organs function. To make things more complicated, even small tissues have millions of component parts, meaning I must develop new specialized software tools, which use images of tissues, and machine learning to create my schematics. These tools simultaneously look at millions of cells and distill their information down to a few key, understandable relationships. This helps us understand where different types of cells are, how they are interacting with each other, and what the structure of their host tissue is.

### What public benefit do you hope will come from your work?

A better understanding of how the spatial organization of cells influences disease is yielding advances on both the basic research and clinical levels. Our custom analysis pipelines are shaping the forefront of immunological drug discovery, tissue exploration, and diagnostics.